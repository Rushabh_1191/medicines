package com.medicine.assignment.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.medicine.assignment.fragmentes.MedicineInformationFragment;
import com.medicine.assignment.models.MedicineModel;

import java.util.ArrayList;

/**
 * Created by rushabh on 23/12/15.
 */
public class MedicinePagerAdapter extends FragmentPagerAdapter {


    ArrayList<MedicineModel > medicineList;
    public MedicinePagerAdapter(FragmentManager fm,ArrayList<MedicineModel> models) {
        super(fm);
        medicineList=models;
    }

    @Override
    public Fragment getItem(int position) {

        MedicineInformationFragment fragment=new MedicineInformationFragment();

        Bundle bundle=new Bundle();
        bundle.putSerializable(MedicineInformationFragment.ARG_MEDIINE_INFO,medicineList.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return medicineList.size();
    }
}
