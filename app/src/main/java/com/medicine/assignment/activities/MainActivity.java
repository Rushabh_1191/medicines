package com.medicine.assignment.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.medicine.R;
import com.medicine.assignment.adapter.MedicinePagerAdapter;
import com.medicine.assignment.databasemanager.DatabaseManager;
import com.medicine.assignment.models.MedicineModel;
import com.medicine.assignment.networkrequest.VolleyRequest;
import com.medicine.assignment.networkrequest.VolleyResponseListener;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements VolleyResponseListener, ViewPager.OnPageChangeListener {


    @Bind(R.id.viewpager)
    ViewPager mViewPager;
    ArrayList<MedicineModel> medicineModelArrayList=new ArrayList<>();

    MedicinePagerAdapter adapter;

    ProgressDialog progressDialog;

    @Bind(R.id.pb_progress)
    ProgressBar progressBar;

    ArrayList<String> imageUrls=new ArrayList<>();
    int limit=10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
//        DatabaseManager databaseManager=DatabaseManager.getDatabaseInstance(this);
//        DatabaseManager.releaseDatabase();

        File cacheDir = StorageUtils.getCacheDirectory(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).
                memoryCache(new LruMemoryCache(2 * 1024 * 1024)).memoryCacheSize(2 * 1024 * 1024).
                diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100).build();



        ImageLoader.getInstance().init(config);

        adapter=new MedicinePagerAdapter(getSupportFragmentManager(),medicineModelArrayList);

        mViewPager.setAdapter(adapter);
        fetchMedicinesFromDatabase();

        mViewPager.addOnPageChangeListener(this);

        imageUrls.add("http://8020.photos.jpgmag.com/3799736_236369_4042d98607_m.jpg");
        imageUrls.add("http://8020.photos.jpgmag.com/3799725_322556_e00db2183b_m.jpg");
        imageUrls.add("http://8020.photos.jpgmag.com/3799717_253174_ae0695c05e_m.jpg");
    }

    void fetchMedicinesFromDatabase(){

        DatabaseManager databaseManager=DatabaseManager.getDatabaseInstance(this);

        ArrayList<MedicineModel> list=databaseManager.getAllMedicines(medicineModelArrayList.size(), limit);
        medicineModelArrayList.addAll(list);
        if(medicineModelArrayList.size()==0){
            fetchMedicinesFromServer();
        }
        else {

            adapter.notifyDataSetChanged();
        }
    }

    void fetchMedicinesFromServer(){
        progressDialog=ProgressDialog.show(this,"","Fetching Medicines");
        new VolleyRequest(1,this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return false;
    }

    @Override
    public void responseRecieved(final JSONObject jsonObject, int requestId) {
        final JSONArray medicineList = jsonObject.optJSONArray("result");

        // Starting async task add around 4000 entries int DB.
        new AsyncTask<Void,Integer,Integer>(){

            int numberOfMedicines;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                numberOfMedicines=medicineList.length();


                progressDialog.dismiss();
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setMax(numberOfMedicines);
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);

                progressBar.setProgress(values[0]);
            }

            @Override
            protected Integer doInBackground(Void... params) {
                try {




                    Gson gson = new Gson();
                    DatabaseManager databaseManager = DatabaseManager.getDatabaseInstance(getApplicationContext());
                    for (int i = 0; i < numberOfMedicines; i++) {

                        MedicineModel medicineModel = gson.fromJson(medicineList.getString(i), MedicineModel.class);
                        medicineModel.imgUrl=imageUrls.get(i%3);// Hardcoding image urls
                        publishProgress(i);
                        databaseManager.addMedicine(medicineModel);
                    }

                    DatabaseManager.releaseDatabase();
                }
                catch (Exception e){

                }
                return null;
            }

            @Override
            protected void onPostExecute(Integer integer) {
                super.onPostExecute(integer);
                progressBar.setVisibility(View.GONE);
                fetchMedicinesFromDatabase();
            }

        }.execute();



    }

    @Override
    public void errorRecieved(VolleyError error, int requestId) {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if(position==(mViewPager.getAdapter().getCount()-3)){
            fetchMedicinesFromDatabase();
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
