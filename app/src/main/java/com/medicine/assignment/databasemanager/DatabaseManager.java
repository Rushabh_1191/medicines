package com.medicine.assignment.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.medicine.assignment.models.MedicineModel;

import java.util.ArrayList;

/**
 * Created by rushabh on 14/10/14.
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private static int instanceCounter;
    private static DatabaseManager databaseInstance;
    private static SQLiteDatabase mDB;

    final static String DATABASE_NAME="com.medicine";
    final static int VERSION=1;


    public final static String TABLE_MEDICINE="table_medicine_info";
    public static final String C_ID="_id";
    public static final String C_MEDCINE_INFO="medicine_info";
    private DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        mDB = getWritableDatabase();

    }


    public synchronized static DatabaseManager getDatabaseInstance(Context context) {


        if (databaseInstance == null) {
            databaseInstance = new DatabaseManager(context);
        }
        ++instanceCounter;

        return databaseInstance;
    }


    public synchronized static void releaseDatabase() {


        instanceCounter--;
        if (instanceCounter < 0) {
            instanceCounter=0;
            if(mDB!=null) {
                mDB.close();
                databaseInstance.close();
                databaseInstance = null;
                mDB = null;
            }

        }
    }




    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String createRegisterAddaTable = "CREATE TABLE "
                + TABLE_MEDICINE + "(" + C_ID + " integer AUTO INCREMENT," + C_MEDCINE_INFO
                + " text)";
        sqLiteDatabase.execSQL(createRegisterAddaTable);


    }

    public long addMedicine(MedicineModel model){

        Gson gson=new Gson();

        ContentValues contentValues=new ContentValues();
        contentValues.put(C_MEDCINE_INFO, gson.toJson(model));
        long id=mDB.insert(TABLE_MEDICINE,null,contentValues);

        return id;
    }


    public ArrayList<MedicineModel> getAllMedicines(int skip,int limit){
        ArrayList<MedicineModel> listOfMedicines=new ArrayList<>();

        String query="SELECT * FROM "+TABLE_MEDICINE+" LIMIT "+skip+","+limit;
        Cursor cursor=mDB.rawQuery(query,null);

        Gson gson=new Gson();
        while (cursor.moveToNext()){

            MedicineModel medicineModel=gson.fromJson
                    (cursor.getString(cursor.getColumnIndex(C_MEDCINE_INFO))
                            , MedicineModel.class);
            listOfMedicines.add(medicineModel);
        }
        return listOfMedicines;
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {



    }


}
