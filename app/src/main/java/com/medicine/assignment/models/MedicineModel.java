
package com.medicine.assignment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class MedicineModel implements Serializable{

    @SerializedName("su")
    @Expose
    public Integer su;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("slug")
    @Expose
    public String slug;
    @SerializedName("form")
    @Expose
    public String form;
    @SerializedName("mfId")
    @Expose
    public Integer mfId;
    @SerializedName("oPrice")
    @Expose
    public float oPrice;
    @SerializedName("hkpDrugCode")
    @Expose
    public Integer hkpDrugCode;
    @SerializedName("uip")
    @Expose
    public float uip;
    @SerializedName("discountPerc")
    @Expose
    public Integer discountPerc;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("uPrice")
    @Expose
    public Double uPrice;
    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("available")
    @Expose
    public Boolean available;
    @SerializedName("packSize")
    @Expose
    public String packSize;
    @SerializedName("pForm")
    @Expose
    public String pForm;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("productsForBrand")
    @Expose
    public Object productsForBrand;
    @SerializedName("generics")
    @Expose
    public Object generics;
    @SerializedName("mrp")
    @Expose
    public float mrp;
    @SerializedName("manufacturer")
    @Expose
    public String manufacturer;
    @SerializedName("imgUrl")
    @Expose
    public String imgUrl;
    @SerializedName("packForm")
    @Expose
    public String packForm;



}
