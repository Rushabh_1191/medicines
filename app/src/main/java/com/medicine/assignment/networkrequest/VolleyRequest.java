package com.medicine.assignment.networkrequest;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.medicine.assignment.MedicineApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class VolleyRequest {


    public static String TAG = "volley";


    private VolleyResponseListener onVolleyResponse;

    public VolleyRequest(  final int requestId,  final VolleyResponseListener listener) {

        MedicineApplication app = MedicineApplication.getInstance();

        StringRequest volleyRequest = createRequest(requestId,listener);
        app.addRequest(volleyRequest, requestId + "");

    }

    public VolleyRequest() {

    }

    public StringRequest createRequest( final int requestId, final VolleyResponseListener listener) {

        onVolleyResponse = listener;

        String finalUrl = "https://www.1mg.com/api/v1/search/autocomplete?name=b&pageSize=10000000&_=1435404923427";


        StringRequest request = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (onVolleyResponse != null) {


                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        onVolleyResponse.responseRecieved(jsonObject, requestId);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();

                        if (onVolleyResponse != null)
                            onVolleyResponse.errorRecieved(error, requestId);
                    }
                });


        request.setShouldCache(false);

        int socketTimeout = 30000;
        request.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return request;
    }
}
