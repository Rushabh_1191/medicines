package com.medicine.assignment.fragmentes;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.medicine.R;
import com.medicine.assignment.models.MedicineModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicineInformationFragment extends Fragment {

    @Bind(R.id.tv_medicine_name)
    TextView tvMedicineName;

    @Bind(R.id.tv_medicine_label)
    TextView tvMedicineLabel;

    @Bind(R.id.tv_mfg_by)
    TextView tvMfgBy;

    @Bind(R.id.tv_pack_size)
    TextView tvPackSize;

    @Bind(R.id.tv_pack_form)
    TextView tvPackForm;

    @Bind(R.id.tv_mrp)
    TextView tvMrp;

    @Bind(R.id.tv_medicine_type)
    TextView tvTypeOfMedicine;

    @Bind(R.id.tv_order_placed)
    TextView tvOrderPlaced;

    @Bind(R.id.iv_medicine_image)
    ImageView ivMedicineImage;


    public static final String ARG_MEDIINE_INFO="medicine_info";

    public MedicineInformationFragment() {
        // Required empty public constructor
    }

    MedicineModel medicineModel;


    ImageLoader imageLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle=getArguments();
        medicineModel= (MedicineModel) bundle.getSerializable(ARG_MEDIINE_INFO);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_medicine_information, container, false);

        ButterKnife.bind(this, view);

        imageLoader= ImageLoader.getInstance();
        DisplayImageOptions imageDisplayOptions = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .showImageOnLoading(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .build();

        tvMedicineName.setText(medicineModel.name);
        tvMedicineLabel.setText(medicineModel.label);
        tvMfgBy.setText(medicineModel.manufacturer);
        tvPackSize.setText(medicineModel.packSize);
        tvPackForm.setText(medicineModel.packForm);
        tvMrp.setText("MRP " + medicineModel.mrp + " /-");
        tvTypeOfMedicine.setText(medicineModel.type);

        imageLoader.displayImage(medicineModel.imgUrl,ivMedicineImage,imageDisplayOptions);


        return view;
    }

    @OnClick(R.id.btn_buy)
    void buyMedicine(){
        Animation animation=AnimationUtils.loadAnimation(getActivity(),android.R.anim.fade_in);
        tvOrderPlaced.setAnimation(animation);
        tvOrderPlaced.setVisibility(View.VISIBLE);
        animation.start();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
